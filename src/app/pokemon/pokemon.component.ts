import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import {PokemonService} from "./pokemon.service";

@Component({
  selector: 'app-pokemon',
  templateUrl: './pokemon.component.html',
  styleUrls: ['pokemon.scss']
})
export class PokemonComponent implements OnInit {

  pokemons: any[] = [];
  page: number = 1;
  pages: number = 1;

  constructor(protected pokemonService: PokemonService) { }

  ngOnInit() {
    this.loadAll();
  }

  loadAll(page: number = 1) {
    this.pokemonService
      .query(page)
      .subscribe(
        (res: any) => {
          this.pokemons = res.body.results;
          this.pages = Math.ceil(res.body.count / 20);
          },
        error => {console.log(error);
      });
  }
}
