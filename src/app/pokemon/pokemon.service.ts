import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class PokemonService {
  public resourceUrl = 'http://pokeapi-env.eba-hsrnc62d.us-east-1.elasticbeanstalk.com';

  constructor(protected http: HttpClient) {}

  find(id: number): Observable<any> {
    return this.http.get<any>(`${this.resourceUrl}/pokemon/${id}`, { observe: 'response' });
  }

  query(page?: any): Observable<any> {
    return this.http.get<any>(`${this.resourceUrl}/pokemon` + (page ? `?page=` + (page-1) : ''), { observe: 'response' });
  }
}
