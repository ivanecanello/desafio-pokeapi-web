import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, ActivatedRouteSnapshot} from '@angular/router';
import {PokemonService} from "./pokemon.service";

@Component({
  selector: 'app-pokemon-detail',
  templateUrl: './pokemon-detail.component.html',
  styleUrls: ['pokemon.scss']
})
export class PokemonDetailComponent implements OnInit {
  id: number;
  pokemon: any = {};

  constructor(protected activatedRoute: ActivatedRoute, protected pokemonService: PokemonService) {
    this.id = activatedRoute.snapshot.params['id']
  }

  ngOnInit() {
    this.loadPokemon()
  }

  loadPokemon() {
    this.pokemonService
      .find(this.id)
      .subscribe((res: any) => this.pokemon = res.body);
  }

  previousState() {
    window.history.back();
  }
}
