import { Routes } from '@angular/router';
import { PokemonComponent } from "./pokemon.component";
import {PokemonDetailComponent} from "./pokemon-detail.component";

export const pokemonRoute: Routes = [
  {
    path: '',
    component: PokemonComponent,
  },
  {
    path: 'pokemon/:id',
    component: PokemonDetailComponent
  },
];
