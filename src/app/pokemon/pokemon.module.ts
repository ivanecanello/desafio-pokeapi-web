import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import {pokemonRoute} from "./pokemon.route";
import {PokemonComponent} from "./pokemon.component";
import {PokemonDetailComponent} from "./pokemon-detail.component";
import {CommonModule} from "@angular/common";
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {PaginationComponent} from "../shared/pagination/pagination.component";

const ENTITY_STATES = [...pokemonRoute];

@NgModule({
  imports: [RouterModule.forChild(ENTITY_STATES), CommonModule, NgbModule],
  declarations: [PokemonComponent, PokemonDetailComponent, PaginationComponent]
})
export class PokemonModule {}
