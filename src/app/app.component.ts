import {Component, OnInit} from '@angular/core';
import {PokemonService} from "./pokemon/pokemon.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent{

  title: string = 'pokeapi';

}
