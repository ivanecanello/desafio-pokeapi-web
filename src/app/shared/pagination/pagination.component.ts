import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { BreakpointObserver, BreakpointState } from '@angular/cdk/layout';

@Component({
  selector: 'app-pagination',
  template: `<nav *ngIf="pages.length > 0">
    <ul class="pagination justify-content-center">
      <li *ngIf="page > 1" class="page-item">
        <button (click)="setPage(page - 1)" class="page-link  d-none d-lg-block" aria-label="Previous">
          <span aria-hidden="true">&laquo;</span>
        </button>
      </li>

      <li *ngIf="moreLeft" class="page-item">
        <button (click)="setPage(pages[0] - 1)" class="page-link" aria-label="Next">
          <span aria-hidden="true">...</span>
        </button>
      </li>

      <li *ngFor="let p of pages; i as index" class="page-item" [ngClass]="{ active: p === page }">
        <button class="page-link" (click)="setPage(p)">{{ p }}</button>
      </li>

      <li *ngIf="moreRight" class="page-item">
        <button (click)="setPage(pages[pages.length - 1] + 1)" class="page-link" aria-label="Next">
          <span aria-hidden="true">...</span>
        </button>
      </li>

      <li *ngIf="page < pages[pages.length - 1]" class="page-item d-none d-lg-block">
        <button (click)="setPage(page + 1)" class="page-link" aria-label="Next">
          <span aria-hidden="true">&raquo;</span>
        </button>
      </li>
    </ul>
  </nav>`,
})
export class PaginationComponent implements OnInit {
  pages: number[] = [];
  moreRight: boolean = false;
  moreLeft: boolean = false;
  leftLength = 5;
  rightLength = 5;

  @Input('pages') pagesLength: number = 0;
  @Input() page: number = 1;

  @Output() pushPages = new EventEmitter();

  ngOnChanges() {
    this.setPages();
  }

  constructor(public breakPointObserver: BreakpointObserver) {}

  ngOnInit() {
    this.breakPointObserver.observe(['(max-width: 768px)']).subscribe((state: BreakpointState) => {
      if (state.matches) {
        this.rightLength = 2;
        this.leftLength = 3;
        this.setPages();
      } else {
        this.rightLength = 5;
        this.leftLength = 5;
        this.setPages();
      }
    });
  }

  setPage(p: number) {
    this.page = p;
    this.pushPages.emit(this.page);
  }

  setPages() {
    this.pages = [];
    let start = this.page - this.rightLength;
    let end = this.page + this.leftLength;

    if (start < 1) {
      end += start * -1;
      start = 1;
    }

    if (start > 1) this.moreLeft = true;
    else this.moreLeft = false;

    if (end < this.pagesLength) this.moreRight = true;
    else this.moreRight = false;

    if (end > this.pagesLength) end = this.pagesLength;
    for (let i = start; i <= end; i++) {
      this.pages.push(i);
    }
  }
}
